package com.hackspace.alex.worklibrary.view;

public interface IProgressBehavior {
    void showProgress();
    void hideProgress();
}
