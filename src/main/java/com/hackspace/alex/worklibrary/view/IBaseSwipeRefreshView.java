package com.hackspace.alex.worklibrary.view;


public interface IBaseSwipeRefreshView {
    void setRefreshing(boolean refreshing);
}
