package com.hackspace.alex.worklibrary.view;

public interface IBaseView extends IProgressBehavior, IBaseSwipeRefreshView {
    void showToast(String title);
}
