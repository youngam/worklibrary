package com.hackspace.alex.worklibrary.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.hackspace.alex.worklibrary.ui.dialog.ProgressDialogFragment;
import com.hackspace.alex.worklibrary.view.IBaseView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseAppCompatActivity extends AppCompatActivity implements IBaseView {
    private ProgressDialogFragment mProgressDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    protected void initSwipeRefreshLayout(int refreshLayoutId,
                                          SwipeRefreshLayout.OnRefreshListener refreshListener) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(refreshLayoutId);
        mSwipeRefreshLayout.setDistanceToTriggerSync(200);
        mSwipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.YELLOW, Color.RED);
        mSwipeRefreshLayout.setOnRefreshListener(refreshListener);
    }

    public void setRefreshing(boolean refreshing) {
        mSwipeRefreshLayout.setRefreshing(refreshing);
    }


    @Override
    public void showProgress() {
        mProgressDialog.show(getSupportFragmentManager(), ProgressDialogFragment.TAG);
    }

    @Override
    public void hideProgress() {
        mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressDialog = new ProgressDialogFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void showToast(String title) {

    }
}