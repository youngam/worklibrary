package com.hackspace.alex.worklibrary.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hackspace.alex.worklibrary.R;
import com.hackspace.alex.worklibrary.ui.utils.Font;

public class TextTextView extends LinearLayout {
    private TextView mTextView1;
    private TextView mTextView2;
    private TextStyle mText1Style = TextStyle.NORMAL;
    private TextStyle mText2Style = TextStyle.NORMAL;

    public TextTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        boolean hasCustomLayout = isInEditMode() ? false : setCustomLayout(context, attrs);

        if(!hasCustomLayout) LinearLayout.inflate(getContext(), R.layout.text_text_layout, this);
        //
        initViews();
        if (isInEditMode()) initTestData();

        initViewAttrs(attrs);
    }

    public TextTextView(Context context) {
        super(context);
        LinearLayout.inflate(getContext(), R.layout.text_text_layout, this);
        initViews();
    }

    private void initViews() {
        mTextView1 = (TextView) findViewById(R.id.text_view_1);
        mTextView2 = (TextView) findViewById(R.id.text_view_2);
    }

    private void initTestData() {
        mTextView1.setText("Text#1");
        mTextView2.setText("Text#2");
    }

    private boolean setCustomLayout(Context context, AttributeSet attrs){
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextTextView);
        int resId = -1;
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attrIndex = a.getIndex(i);
            if (attrIndex == R.styleable.TextTextView_customLayout) {
                resId = a.getResourceId(attrIndex, -1);
                inflate(context, resId, this);
            }
        }
        return (resId != -1);
    }

    private void initViewAttrs(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextTextView);
        LinearLayout.LayoutParams text1Params = (LinearLayout.LayoutParams) mTextView1.getLayoutParams();
        LinearLayout.LayoutParams text2Params = (LinearLayout.LayoutParams) mTextView2.getLayoutParams();

        CharSequence text;
        ColorStateList color;
        float textSize;
        int typeFaceIndex;
        Drawable background;

        int text1LeftMargin = text1Params.leftMargin;
        int text1TopMargin = text1Params.topMargin;
        int text1RightMargin = text1Params.rightMargin;
        int text1BottomMargin = text1Params.bottomMargin;

        int text2LeftMargin = text2Params.leftMargin;
        int text2TopMargin = text2Params.topMargin;
        int text2RightMargin = text2Params.rightMargin;
        int text2BottomMargin = text2Params.bottomMargin;

        int text1PaddingLeft = mTextView1.getPaddingLeft();
        int text1PaddingTop = mTextView1.getPaddingTop();
        int text1PaddingRight = mTextView1.getPaddingRight();
        int text1PaddingBottom = mTextView1.getPaddingBottom();

        int text2PaddingLeft = mTextView2.getPaddingLeft();
        int text2PaddingTop = mTextView2.getPaddingTop();
        int text2PaddingRight = mTextView2.getPaddingRight();
        int text2PaddingBottom = mTextView2.getPaddingBottom();

        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attrIndex = a.getIndex(i);
            if (attrIndex == R.styleable.TextTextView_text1) {
                text = a.getText(attrIndex);
                mTextView1.setText(text);
            } else if (attrIndex == R.styleable.TextTextView_text2) {
                text = a.getText(attrIndex);
                mTextView2.setText(text);
            } else if (attrIndex == R.styleable.TextTextView_text1Color) {
                color = a.getColorStateList(attrIndex);
                mTextView1.setTextColor(color);
            } else if (attrIndex == R.styleable.TextTextView_text1Style) {
                int text1Style = a.getInteger(attrIndex, 0);
                mText1Style = TextStyle.getByIndex(text1Style);
                setTextView1TextStyle(mText1Style);
            } else if (attrIndex == R.styleable.TextTextView_text2Color) {
                color = a.getColorStateList(attrIndex);
                mTextView2.setTextColor(color);
            } else if (attrIndex == R.styleable.TextTextView_text1Size) {
                textSize = a.getDimensionPixelSize(attrIndex, 0);
                if (textSize != 0) mTextView1.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else if (attrIndex == R.styleable.TextTextView_text2Size) {
                textSize = a.getDimensionPixelSize(attrIndex, 0);
                if (textSize != 0) mTextView2.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else if (attrIndex == R.styleable.TextTextView_text2Style) {
                int text2Style = a.getInteger(attrIndex, 0);
                mText2Style = TextStyle.getByIndex(text2Style);
                setTextView2TextStyle(mText2Style);
            } else if (attrIndex == R.styleable.TextTextView_textsStyle) {
                int textsStyle = a.getInteger(attrIndex, 0);
                if (textsStyle != 0) {
                    mText1Style = mText2Style = TextStyle.getByIndex(textsStyle);
                }
            } else if (attrIndex == R.styleable.TextTextView_textsColor) {
                ColorStateList stateColor = a.getColorStateList(attrIndex);
                mTextView1.setTextColor(stateColor);
                mTextView2.setTextColor(stateColor);
            } else if (attrIndex == R.styleable.TextTextView_textsSize) {
                textSize = a.getDimension(attrIndex, 0);
                if (textSize != 0) {
                    mTextView1.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                    mTextView2.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                }
            } else if (attrIndex == R.styleable.TextTextView_text1Background) {
                background = a.getDrawable(attrIndex);
                setText1Background(background);
            } else if (attrIndex == R.styleable.TextTextView_text2Background) {
                background = a.getDrawable(attrIndex);
                setText2Background(background);
            }else if (attrIndex == R.styleable.TextTextView_text1TypeFace) {
                typeFaceIndex = a.getInt(attrIndex, -1);
                if (typeFaceIndex != -1 && !isInEditMode()) { // Font is ignorable in EditMode
                    mTextView1.setTypeface(Font.getByIndex(typeFaceIndex, getContext()));
                }
            } else if (attrIndex == R.styleable.TextTextView_text2TypeFace) {
                typeFaceIndex = a.getInt(attrIndex, -1);
                if (typeFaceIndex != -1 && !isInEditMode()) { // Font is ignorable in EditMode
                    mTextView2.setTypeface(Font.getByIndex(typeFaceIndex, getContext()));
                }
            } else if (attrIndex == R.styleable.TextTextView_text1Width) {
                int txt1Width = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1Width != -1) {
                    text1Params.width = txt1Width;
                }
            } else if (attrIndex == R.styleable.TextTextView_text2Width) {
                int txt1Width = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1Width != -1) {
                    text2Params.width = txt1Width;
                }
            } else if (attrIndex == R.styleable.TextTextView_gravity) {
                setGravity(a.getInt(attrIndex, -1));
            } else if (attrIndex == R.styleable.TextTextView_text1Margin) {
                int txt1margin = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1margin != -1) {
                    text1LeftMargin = txt1margin;
                    text1TopMargin = txt1margin;
                    text1RightMargin = txt1margin;
                    text1BottomMargin = txt1margin;
                }
            } else if (attrIndex == R.styleable.TextTextView_text1MarginBottom) {
                int txt1marginBottom = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1marginBottom != -1) {
                    text1BottomMargin = txt1marginBottom;
                }
            } else if (attrIndex == R.styleable.TextTextView_text1Gravity) {
                mTextView1.setGravity(a.getInt(attrIndex, -1));
            } else if (attrIndex == R.styleable.TextTextView_text1MarginTop) {
                int txt1marginTop = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1marginTop != -1) {
                    text1TopMargin = txt1marginTop;
                }
            } else if (attrIndex == R.styleable.TextTextView_text1MarginLeft) {
                int txt1marginLeft = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1marginLeft != -1) {
                    text1LeftMargin = txt1marginLeft;
                }
            } else if (attrIndex == R.styleable.TextTextView_text1MarginRight) {
                int txt1marginRight = a.getDimensionPixelSize(attrIndex, -1);
                if (txt1marginRight != -1) {
                    text1RightMargin = txt1marginRight;
                }
            } else if (attrIndex == R.styleable.TextTextView_text2Margin) {
                int txt2margin = a.getDimensionPixelSize(attrIndex, -1);
                if (txt2margin != -1) {
                    text2LeftMargin = txt2margin;
                    text2TopMargin = txt2margin;
                    text2RightMargin = txt2margin;
                    text2BottomMargin = txt2margin;
                }
            } else if (attrIndex == R.styleable.TextTextView_text2MarginBottom) {
                int txt2marginBottom = a.getDimensionPixelSize(attrIndex, -1);
                if (txt2marginBottom != -1) {
                    text2BottomMargin = txt2marginBottom;
                }
            } else if (attrIndex == R.styleable.TextTextView_text2MarginTop) {
                int txt2marginTop = a.getDimensionPixelSize(attrIndex, -1);
                if (txt2marginTop != -1) {
                    text2TopMargin = txt2marginTop;
                }
            } else if (attrIndex == R.styleable.TextTextView_text2MarginLeft) {
                int txt2marginLeft = a.getDimensionPixelSize(attrIndex, -1);
                if (txt2marginLeft != -1) {
                    text2LeftMargin = txt2marginLeft;
                }
            } else if (attrIndex == R.styleable.TextTextView_text2MarginRight) {
                int txt2marginRight = a.getDimensionPixelSize(attrIndex, -1);
                if (txt2marginRight != -1) {
                    text2RightMargin = txt2marginRight;
                }
            } else if (attrIndex == R.styleable.TextTextView_text1SingleLine) {
                boolean text1SingleLine = a.getBoolean(attrIndex, false);
                setTextSingleLine(mTextView1, text1SingleLine);
            } else if (attrIndex == R.styleable.TextTextView_text2SingleLine) {
                boolean text2SingleLine = a.getBoolean(attrIndex, false);
                setTextSingleLine(mTextView2, text2SingleLine);
            } else if (attrIndex == R.styleable.TextTextView_text2RepeatSingleLine) {
                boolean text2RepeatSingleLine = a.getBoolean(attrIndex, false);
                setText2RepeatSingleLine(text2RepeatSingleLine);
            } else if (attrIndex == R.styleable.TextTextView_text1HorizontalScrolling) {
                boolean text1HorizontalScrolling = a.getBoolean(attrIndex, false);
                setHorizontalScrolling(mTextView1, text1HorizontalScrolling);
            } else if (attrIndex == R.styleable.TextTextView_text2HorizontalScrolling) {
                boolean text2HorizontalScrolling = a.getBoolean(attrIndex, false);
                setHorizontalScrolling(mTextView2, text2HorizontalScrolling);
            } else if (attrIndex == R.styleable.TextTextView_text2Gravity) {
                mTextView2.setGravity(a.getInt(attrIndex, -1));
            } else if (attrIndex == R.styleable.TextTextView_textsAllCaps) {
                boolean textsAllCaps = a.getBoolean(attrIndex, false);
                mTextView1.setAllCaps(textsAllCaps);
                mTextView2.setAllCaps(textsAllCaps);
            } else if (attrIndex == R.styleable.TextTextView_text1AllCaps) {
                boolean text1AllCaps = a.getBoolean(attrIndex, false);
                mTextView1.setAllCaps(text1AllCaps);
            } else if (attrIndex == R.styleable.TextTextView_text2AllCaps) {
                boolean text2AllCaps = a.getBoolean(attrIndex, false);
                mTextView2.setAllCaps(text2AllCaps);
            }else if (attrIndex == R.styleable.TextTextView_textsHeight) {
                int textsHeight = a.getDimensionPixelSize(attrIndex, 0);
                if (textsHeight != 0) {
                    text1Params.height = text2Params.height = textsHeight;
                }
            } else if (attrIndex == R.styleable.TextTextView_text1Padding){
                text1PaddingLeft = text1PaddingTop = text1PaddingRight = text1PaddingBottom = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text1PaddingLeft){
                text1PaddingLeft = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text1PaddingTop){
                text1PaddingTop = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text1PaddingRight){
                text1PaddingRight = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text1PaddingBottom){
                text1PaddingBottom = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text2Padding){
                text2PaddingLeft = text2PaddingTop = text2PaddingRight = text2PaddingBottom = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text2PaddingLeft){
                text2PaddingLeft = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text2PaddingRight){
                text2PaddingRight = a.getDimensionPixelSize(attrIndex, 0);
            } else if (attrIndex == R.styleable.TextTextView_text1MinHeight){
                mTextView1.setMinHeight(a.getDimensionPixelSize(attrIndex, 0));
            } else if (attrIndex == R.styleable.TextTextView_text1MinWidth){
                mTextView1.setMinWidth(a.getDimensionPixelSize(attrIndex, 0));
            } else if (attrIndex == R.styleable.TextTextView_text2MinWidth){
                mTextView2.setMinWidth(a.getDimensionPixelSize(attrIndex, 0));
            } else if (attrIndex == R.styleable.TextTextView_text2MinHeight){
                mTextView2.setMinHeight(a.getDimensionPixelSize(attrIndex, 0));
            } else if (attrIndex == R.styleable.TextTextView_textsMinHeight){
                mTextView1.setMinHeight(a.getDimensionPixelSize(attrIndex, 0));
                mTextView2.setMinHeight(a.getDimensionPixelSize(attrIndex, 0));
            } else {
            }
            setText1MarginParam(text1LeftMargin, text1TopMargin, text1RightMargin, text1BottomMargin);
            setText2MarginParam(text2LeftMargin, text2TopMargin, text2RightMargin, text2BottomMargin);


            setText1Padding(text1PaddingLeft, text1PaddingTop, text1PaddingRight, text1PaddingBottom);
            setText2Padding(text2PaddingLeft, text2PaddingTop, text2PaddingRight, text2PaddingBottom);
        }
        a.recycle();
    }

    public void setText2RepeatSingleLine(boolean shouldTextRepeat) {
        mTextView2.setSingleLine(shouldTextRepeat);
        if(shouldTextRepeat){
            //-1 to repeat indefinitely
            mTextView2.setMarqueeRepeatLimit(-1);
            mTextView2.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            mTextView2.setSelected(true);
        }
    }

    public void setText1Background(Drawable background) {
        mTextView1.setBackground(background);
    }

    public void setText2Background(Drawable background) {
        mTextView2.setBackground(background);
    }

    public void setText1BackgroundResource(int resId) {
        mTextView1.setBackgroundResource(resId);
    }

    public void setText2BackgroundResource(int resId) {
        mTextView2.setBackgroundResource(resId);
    }

    private void setText1Padding(int left, int top, int right, int bottom) {
        mTextView1.setPadding(left, top, right, bottom);
    }

    private void setText2Padding(int left, int top, int right, int bottom) {
        mTextView2.setPadding(left, top, right, bottom);
    }

    private void setTextSingleLine(TextView textView, boolean isSingleLine) {
        textView.setMaxLines(isSingleLine ? 1 : Integer.MAX_VALUE);
        textView.setEllipsize(TextUtils.TruncateAt.END);
    }

    private void setHorizontalScrolling(TextView textView, boolean isHorizontalScrolling) {
        textView.setHorizontallyScrolling(isHorizontalScrolling);
        textView.setHorizontalFadingEdgeEnabled(isHorizontalScrolling);
        if(isHorizontalScrolling) textView.setMovementMethod(new ScrollingMovementMethod());
    }

    public void setText1MarginParam(int leftMargin, int topMargin, int rightMargin, int bottomMargin) {
        // substitute parameters for left, top, right, bottom
        LinearLayout.LayoutParams text1Params = (LinearLayout.LayoutParams) mTextView1.getLayoutParams();
        text1Params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        mTextView1.setLayoutParams(text1Params);
    }

    private void setText2MarginParam(int leftMargin, int topMargin, int rightMargin, int bottomMargin) {
        // substitute parameters for left, top, right, bottom
        LinearLayout.LayoutParams text2Params = (LinearLayout.LayoutParams) mTextView2.getLayoutParams();
        text2Params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
        mTextView2.setLayoutParams(text2Params);
    }

    public CharSequence getText1() {
        return mTextView1.getText();
    }
    public CharSequence getText2() {
        return mTextView2.getText();
    }

    public void setText1(CharSequence text) {
        mTextView1.setText(text);
    }

    @StringRes
    public void setText1(int textResId) {
        mTextView1.setText(textResId);
    }

    public void setText2(CharSequence text) {
        mTextView2.setText(text);
    }

    @StringRes
    public void setText2(int textResId) {
        mTextView2.setText(textResId);
    }

    /**
     *
     * @param repeatLimit Sets how many times to repeat the marquee animation. Only applied if the TextView has marquee enabled. Set to -1 to repeat indefinitely.
     * @param where If {@link #setMaxLines} has been used to set two or more lines,
     * only {@link android.text.TextUtils.TruncateAt#END} and
     * {@link android.text.TextUtils.TruncateAt#MARQUEE} are supported
     * (other ellipsizing types will not do anything).
     */
    public void setText2EllipsizeMarquee(int repeatLimit, TextUtils.TruncateAt where){
        mTextView2.setMarqueeRepeatLimit(repeatLimit);
        mTextView2.setEllipsize(where);
    }

    public void setTextView1TextStyle(TextStyle textStyle) {
        switch (textStyle) {
            case NORMAL:
                mTextView1.setTypeface(null, Typeface.NORMAL);
                break;
            case BOLD:
                mTextView1.setTypeface(null, Typeface.BOLD);
                break;
            case ITALIC:
                mTextView1.setTypeface(null, Typeface.ITALIC);
                break;
            default:
                break;
        }
    }

    public void setTextView2TextStyle(TextStyle textStyle) {
        switch (textStyle) {
            case NORMAL:
                mTextView2.setTypeface(null, Typeface.NORMAL);
                break;
            case BOLD:
                mTextView2.setTypeface(null, Typeface.BOLD);
                break;
            case ITALIC:
                mTextView2.setTypeface(null, Typeface.ITALIC);
                break;
            default:
                break;
        }
    }

    public void setText1Color(int resIdTv1) {
        mTextView1.setTextColor(getResources().getColor(resIdTv1));
    }
    public void setText2Color(int resIdTv2) {
        mTextView2.setTextColor(getResources().getColor(resIdTv2));
    }
    public void setTextsColors(int resIdTv1, int resIdTv2) {
        mTextView1.setTextColor(getResources().getColor(resIdTv1));
        mTextView2.setTextColor(getResources().getColor(resIdTv2));
    }

    public void setTextsSize(float pixSizeTv1, float pixSizeTv2) {
        if (pixSizeTv1 != 0 && pixSizeTv2 != 0) {
            mTextView1.setTextSize(TypedValue.COMPLEX_UNIT_PX, pixSizeTv1);
            mTextView2.setTextSize(TypedValue.COMPLEX_UNIT_PX, pixSizeTv2);
        }
    }

    public void setText1Size(float text1Size){
        mTextView1.setTextSize(TypedValue.COMPLEX_UNIT_PX, text1Size);
    }

    public void setText2Size(float text2Size){
        mTextView2.setTextSize(TypedValue.COMPLEX_UNIT_PX, text2Size);
    }

    public void setText1Typeface(Typeface text1Typeface){
        mTextView1.setTypeface(text1Typeface);
    }

    public void setText2Typeface(Typeface text2Typeface){
        mTextView2.setTypeface(text2Typeface);
    }

    public void setText1Visibility(int text1Visibility) {
        mTextView1.setVisibility(text1Visibility);
    }

    public void setText2Visibility(int text2Visibility) {
        mTextView2.setVisibility(text2Visibility);
    }

    public void setTextsVisibility(int text1Visibility, int text2Visibility) {
        mTextView1.setVisibility(text1Visibility);
        mTextView2.setVisibility(text2Visibility);
    }

    public void setTextsGravity(int text1Gravity, int text2Gravity) {
        mTextView1.setGravity(text1Gravity);
        mTextView2.setGravity(text2Gravity);
    }

    public LinearLayout.LayoutParams getText1LayoutParams() {
        return (LinearLayout.LayoutParams) mTextView1.getLayoutParams();
    }

    public LinearLayout.LayoutParams getText2LayoutParams() {
        return (LinearLayout.LayoutParams) mTextView2.getLayoutParams();
    }

    public static enum TextStyle {
        NORMAL, BOLD, ITALIC;

        public static TextStyle getByIndex(int index) {
            return values()[index];
        }
    }
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        mTextView1.setEnabled(enabled);
        mTextView2.setEnabled(enabled);
    }
}
