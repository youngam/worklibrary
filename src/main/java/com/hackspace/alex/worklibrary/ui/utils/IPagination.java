package com.hackspace.alex.worklibrary.ui.utils;

public interface IPagination {
    int NORMAL_SIZE = 20;
    int BIG_SIZE = 50;

    Long getTotalCount();

    Integer getCurrPage();

    Integer getPerPage();

    void addPage();

    void setTotalCount(Long totalCount);

}
