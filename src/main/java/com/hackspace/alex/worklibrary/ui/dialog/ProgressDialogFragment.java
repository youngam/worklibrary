package com.hackspace.alex.worklibrary.ui.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Purpose of the class is to prevent leaked window errors
 * see more https://developer.android.com/reference/android/app/DialogFragment.html
 */
public class ProgressDialogFragment extends DialogFragment {
    public static final String TAG = "progressDialogFragment";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return ProgressDialog.show(getActivity(), "loading", "",false, false);
    }
}