package com.hackspace.alex.worklibrary.ui.helper;

import java.util.Collection;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.hackspace.alex.worklibrary.R;
import com.hackspace.alex.worklibrary.ui.widget.ErrorItemViewHolder;

public abstract class EndlessAdapter<T, V extends RecyclerView.ViewHolder> extends
        BaseRecyclerAdapter<T, RecyclerView.ViewHolder> {

    public static final int LOADING_FOOTER_VIEW = 2;
    public static final int ERROR_FOOTER_VIEW = 3;
    public static final int CONTENT_VIEW = 1;


    private boolean mIsUpdating;
    private boolean mIsError;

    private View.OnClickListener mOnRetryClickListener;

    protected abstract V onCreateItemViewHolder(ViewGroup parent, int viewType);
    protected abstract void onBindItemViewHolder(V holder, int position);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;

        switch (viewType) {

            case CONTENT_VIEW:
                vh = onCreateItemViewHolder(parent, CONTENT_VIEW);
                break;

            case LOADING_FOOTER_VIEW:
                vh = new BaseViewHolder(inflate(R.layout.footer_loading_item_view, parent));
                break;

            case ERROR_FOOTER_VIEW:
                vh = new ErrorItemViewHolder(inflate(R.layout.footer_error_item_view, parent),
                        mOnRetryClickListener);
                break;

            default:
                break;
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(mItems.isEmpty()) return;

        switch (getViewTypeByPosition(position)) {

            case CONTENT_VIEW:
                onBindItemViewHolder((V) holder, position);
                break;

            case LOADING_FOOTER_VIEW:
                //loading item.
                break;

            case ERROR_FOOTER_VIEW:
                //error item
                break;

            default:
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getViewTypeByPosition(position);
    }

    private int getViewTypeByPosition(int position) {

        if (mIsUpdating && position == getItemCount() - 1) {
            return LOADING_FOOTER_VIEW;

        } else if (mIsError && position == getItemCount() - 1) {
            return ERROR_FOOTER_VIEW;

        } else {
            return CONTENT_VIEW;
        }
    }

    public void addItems(Collection<T> items) {
        addItems(items, getItemCount());
    }

    public boolean addItems(Collection<T> items, int location) {
        boolean isAdded = mItems.addAll(location, items);
        notifyItemRangeInserted(location, items.size());
        return isAdded;
    }

    public void setUpdating(boolean isUpdating) {

        if (isUpdating) {
            if (!mItems.contains(null)) mItems.add(null); //add footer
            notifyItemInserted(getItemCount());

        } else {
            mItems.remove(null); //remove footer
            notifyItemRemoved(getItemCount());
        }

        mIsUpdating = isUpdating;
    }

    public void setError(boolean isError, View.OnClickListener onRetryClickListener) {

        if (isError) {
            if (!mItems.contains(null)) mItems.add(null); //add footer
            notifyItemInserted(getItemCount());

        } else {
            mItems.remove(null); //remove footer
            notifyItemRemoved(getItemCount());
        }

        mIsError = isError;
        mOnRetryClickListener = onRetryClickListener;
    }

    public boolean isError() {
        return mIsError;
    }

    public boolean isUpdating() {
        return mIsUpdating;
    }

}
