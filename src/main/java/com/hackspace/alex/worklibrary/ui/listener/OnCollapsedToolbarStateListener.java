package com.hackspace.alex.worklibrary.ui.listener;

public interface OnCollapsedToolbarStateListener {

    void onStateChange(boolean isCollapsed);
}
