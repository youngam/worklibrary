package com.hackspace.alex.worklibrary.ui.listener;

import android.support.design.widget.CollapsingToolbarLayout;

public class HidingCollapsedTitleListener implements OnCollapsedToolbarStateListener{
    private String mTitle;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    public HidingCollapsedTitleListener (String title, CollapsingToolbarLayout collapsingToolbarLayout) {
        mTitle = title;
        mCollapsingToolbarLayout = collapsingToolbarLayout;
    }

    @Override
    public void onStateChange(boolean isCollapsed) {
        mCollapsingToolbarLayout.setTitle(isCollapsed ? mTitle : " "); //careful there should a space between double quote otherwise it wont work
    }
}
