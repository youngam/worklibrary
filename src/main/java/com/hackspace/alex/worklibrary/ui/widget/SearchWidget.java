package com.hackspace.alex.worklibrary.ui.widget;

import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hackspace.alex.worklibrary.R;
import com.hackspace.alex.worklibrary.R2;
import com.hackspace.alex.worklibrary.ui.utils.Font;
import com.hackspace.alex.worklibrary.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

public class SearchWidget extends RelativeLayout {
    @BindView(R2.id.microphone_text_view) TextView mMicrophoneTextView;
    @BindView(R2.id.search_edit_text) EditText mSearchEditText;
    @BindView(R2.id.clear_content_text_view) TextView mClearContentTextView;


    public SearchWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = inflate(context, R.layout.search_widget_layout, this);
        ButterKnife.bind(this, view);
        initView();
        if (!isInEditMode()) initAttrs(attrs);
    }

    private void initAttrs(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SearchWidget);
        Drawable cancelButtonDrawable;
        Drawable searchTextDrawable;
        ColorStateList color;
        CharSequence text;
        int typeFaceIndex;
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attrIndex = a.getIndex(i);
            if (attrIndex == R.styleable.SearchWidget_searchTextSize) {
                int textSize = a.getDimensionPixelSize(attrIndex, 0);
                if (textSize != 0) mSearchEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else if (attrIndex == R.styleable.SearchWidget_searchIconSize) {
                int iconSize = a.getDimensionPixelSize(attrIndex, 0);
                if (iconSize != 0) mClearContentTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, iconSize);
            } else if (attrIndex == R.styleable.SearchWidget_searchTextBackground) {
                searchTextDrawable = a.getDrawable(attrIndex);
                mSearchEditText.setBackground(searchTextDrawable);
            } else if (attrIndex == R.styleable.SearchWidget_clearButtonBackground) {
                cancelButtonDrawable = a.getDrawable(attrIndex);
                mClearContentTextView.setBackground(cancelButtonDrawable);
            } else if (attrIndex == R.styleable.SearchWidget_text1Gravity) {
                mSearchEditText.setGravity(a.getInt(attrIndex, -1));
            } else if (attrIndex == R.styleable.SearchWidget_text2Gravity) {
                mClearContentTextView.setGravity(a.getInt(attrIndex, -1));
            } else if (attrIndex == R.styleable.SearchWidget_text2Color) {
                color = a.getColorStateList(attrIndex);
                mClearContentTextView.setTextColor(color);
            } else if (attrIndex == R.styleable.SearchWidget_searchHintColor) {
                color = a.getColorStateList(attrIndex);
                mSearchEditText.setHintTextColor(color);
            } else if (attrIndex == R.styleable.SearchWidget_searchTextHint) {
                text = a.getText(attrIndex);
                mSearchEditText.setHint(text);
            } else if (attrIndex == R.styleable.SearchWidget_searchTextColor) {
                color = a.getColorStateList(attrIndex);
                mSearchEditText.setTextColor(color);
            } else if (attrIndex == R.styleable.SearchWidget_clearButtonWidth) {
                int cancelButtonWidth = a.getDimensionPixelSize(attrIndex, -1);
                if (cancelButtonWidth != -1) {
                    mClearContentTextView.getLayoutParams().width = cancelButtonWidth;
                }
            } else if (attrIndex == R.styleable.SearchWidget_saveEnabled) {
                mSearchEditText.setSaveEnabled(a.getBoolean(R.styleable.SearchWidget_saveEnabled, true));
            } else if (attrIndex == R.styleable.SearchWidget_clearButtonHeight) {
                int cancelButtonHeight = a.getDimensionPixelSize(attrIndex, -1);
                if (cancelButtonHeight != -1) {
                    mClearContentTextView.getLayoutParams().height = cancelButtonHeight;
                }
            } else if (attrIndex == R.styleable.SearchWidget_searchTextPaddingRight) {
                int paddingRight = a.getDimensionPixelSize(attrIndex, -1);
                if (paddingRight != -1) {
                    mSearchEditText.setPadding(
                            mSearchEditText.getPaddingLeft(),
                            mSearchEditText.getPaddingTop(),
                            paddingRight,
                            mSearchEditText.getPaddingBottom()
                    );
                }
            } else if (attrIndex == R.styleable.SearchWidget_maxLength) {
                int maxLength = a.getInteger(attrIndex, -1);
                if (maxLength != -1) mSearchEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
            } else if (attrIndex == R.styleable.SearchWidget_text1TypeFace) {
                typeFaceIndex = a.getInt(attrIndex, -1);
                if (typeFaceIndex != -1) {
                    mSearchEditText.setTypeface(Font.getByIndex(typeFaceIndex, getContext()));
                }
            } else if (attrIndex == R.styleable.SearchWidget_inputType) {
                mSearchEditText.setInputType(a.getInt(attrIndex, -1));
            }
        }
        a.recycle();
    }

    public void setOnTextChangeListener(Consumer<String> searchQuerySubscriber) {
        PublishSubject<CharSequence> subject = PublishSubject.create();
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mClearContentTextView.setVisibility(charSequence.length() > 0 ? VISIBLE : GONE);
                subject.onNext(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        subject.debounce(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map(String::valueOf)
                .subscribe(searchQuerySubscriber);

        mSearchEditText.addTextChangedListener(textWatcher);
    }

    public void setOnMicrophoneClickListener(OnClickListener onMicrophoneClickListener) {
        mMicrophoneTextView.setOnClickListener(onMicrophoneClickListener);
    }

    private void initView() {
        mClearContentTextView.setOnClickListener(view -> mSearchEditText.getText().clear());
    }

    public void setSearchMode(boolean isSearchMode) {

        mSearchEditText.getText().clear();
        setVisibility(isSearchMode ? VISIBLE : GONE);

        if(isSearchMode) {
            mSearchEditText.requestFocus();
            ViewUtils.showSoftInput(mSearchEditText, getContext());
        }
        else {
            mSearchEditText.clearFocus();
            ViewUtils.hideSoftInput(mSearchEditText, getContext());
        }
    }

    public void setText(String text) {
        mSearchEditText.setText(text);
    }
}
