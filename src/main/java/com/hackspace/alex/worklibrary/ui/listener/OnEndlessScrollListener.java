package com.hackspace.alex.worklibrary.ui.listener;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.hackspace.alex.worklibrary.ui.utils.IPagination;

public abstract class OnEndlessScrollListener extends RecyclerView.OnScrollListener {

    private int visibleThreshold;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int mPaginationSize;

    private int mFooterElement;

    RecyclerView.LayoutManager mLayoutManager;

    public OnEndlessScrollListener(RecyclerView.LayoutManager layoutManager, boolean isThereFooter) {
        this(layoutManager, IPagination.NORMAL_SIZE, isThereFooter);
    }

    public OnEndlessScrollListener(RecyclerView.LayoutManager layoutManager, int paginationSize, boolean isThereFooter) {
        mLayoutManager = layoutManager;
        mPaginationSize = paginationSize;
        visibleThreshold = getVisibleThreshold(layoutManager, paginationSize);
        mFooterElement = isThereFooter? 1 : 0;
    }

    private int getVisibleThreshold(RecyclerView.LayoutManager layoutManager, int paginationSize) {
        int visibleThreshHold = 0;
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            visibleThreshHold = (paginationSize / 2) * ((StaggeredGridLayoutManager)layoutManager).getSpanCount();
        } else if (layoutManager instanceof LinearLayoutManager) {
            visibleThreshHold = paginationSize / 2;
        } else if (layoutManager instanceof GridLayoutManager) {
            visibleThreshHold = (paginationSize / 2) * ((GridLayoutManager)layoutManager).getSpanCount();
        }
        return visibleThreshHold;
    }

    private int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            }
            else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {

        // executes when adapter child range change(adapter.clear(), adapter.setItems())
        // no need to handle this events
        if(dx == 0 && dy == 0) return;

        int lastVisibleItemPosition = 0;
        int totalItemCount = mLayoutManager.getItemCount() - mFooterElement;

        if (mLayoutManager instanceof StaggeredGridLayoutManager) {
            int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mLayoutManager).findLastVisibleItemPositions(null);
            // get maximum element within the list
            lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions);
        } else if (mLayoutManager instanceof LinearLayoutManager) {
            lastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else if (mLayoutManager instanceof GridLayoutManager) {
            lastVisibleItemPosition = ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        }

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                loading = true;
            }
        }
        // If it�s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && (totalItemCount > previousTotalItemCount) && !isLoading()) {
            loading = false;
            previousTotalItemCount = totalItemCount;
        }

        loading = isLoading();
        // If it isn�t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            onLoadMore(totalItemCount);
            loading = true;
        }
    }

    // Defines the process for actually loading more data based on page
    public abstract void onLoadMore(int totalItemsCount);

    public abstract boolean isLoading();

}