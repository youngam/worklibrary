package com.hackspace.alex.worklibrary.ui.listener;

public interface OnItemClickListener<T> {
    void onEntityClick(T entity);
}
