package com.hackspace.alex.worklibrary.ui.helper;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

/**
 * The purpose of this class is to have an access to created fragments for future use
 */
public abstract class StoredFragmentStatePagerAdapter extends FragmentStatePagerAdapter {
    private List<WeakReference<Fragment>> mRegisteredFragments;

    public StoredFragmentStatePagerAdapter(FragmentManager fm) {
        super(fm);
        mRegisteredFragments = new ArrayList<>();
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        mRegisteredFragments.add(new WeakReference<>(fragment));
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mRegisteredFragments.remove(position);
        super.destroyItem(container, position, object);
    }
    
    protected List<Fragment> getRegisteredFragments() {
        return Stream.of(mRegisteredFragments).map(Reference::get).collect(Collectors.toList());
    }
}
