package com.hackspace.alex.worklibrary.ui.listener;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;

public class CollapsedToolbarOffsetChangeListener implements AppBarLayout.OnOffsetChangedListener {
    private boolean mIsShow;
    private int mScrollRange = -1;

    private OnCollapsedToolbarStateListener mCollapsedToolbarStateListener;

    public CollapsedToolbarOffsetChangeListener (String title, CollapsingToolbarLayout toolbarLayout) {
        mCollapsedToolbarStateListener = new HidingCollapsedTitleListener(title, toolbarLayout);
    }

    public CollapsedToolbarOffsetChangeListener(OnCollapsedToolbarStateListener collapsedToolbarStateListener) {
        mCollapsedToolbarStateListener = collapsedToolbarStateListener;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mScrollRange == -1) {
            mScrollRange = appBarLayout.getTotalScrollRange();
        }
        if (mScrollRange + verticalOffset == 0) {
            mIsShow = true;
            mCollapsedToolbarStateListener.onStateChange(true);
        } else if (mIsShow) {
            mIsShow = false;
            mCollapsedToolbarStateListener.onStateChange(false);
        }
    }
}
