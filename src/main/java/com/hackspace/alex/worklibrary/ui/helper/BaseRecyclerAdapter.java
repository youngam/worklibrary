package com.hackspace.alex.worklibrary.ui.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hackspace.alex.worklibrary.ui.listener.OnItemClickListener;

public abstract class BaseRecyclerAdapter<E ,T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {
    protected List<E> mItems = new ArrayList<>();
    private OnItemClickListener<E> mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener<E> onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    protected View inflate(int layoutId, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
    }

    public void setItems(Collection<E> items) {
        if(!mItems.isEmpty()) mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        if(!mItems.isEmpty()) mItems.clear();
        notifyDataSetChanged();
    }

    public List<E> getItems() {
        return mItems;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);

            if(mOnItemClickListener != null) {
                itemView.setOnClickListener(v -> {
                    mOnItemClickListener.onEntityClick(mItems.get(getLayoutPosition()));
                });
            }
        }
    }
}
