package com.hackspace.alex.worklibrary.ui.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hackspace.alex.worklibrary.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ErrorItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.error_message_text_view) TextView mErrorMessageTextView;
    @BindView(R2.id.retry_action_text_view) Button mRetryActionTextView;

    public ErrorItemViewHolder(View itemView, View.OnClickListener onRetryClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mRetryActionTextView.setOnClickListener(onRetryClickListener);
    }
}
