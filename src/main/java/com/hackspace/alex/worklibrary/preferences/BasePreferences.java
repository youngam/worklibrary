package com.hackspace.alex.worklibrary.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.hackspace.alex.worklibrary.BaseApplication;

public class BasePreferences {
    public static final String APP_PREFERENCE = "AppPreference";
    public static final String LOG = "log";

    // ----------- getInt -----------
    public static int getInt(String key) {
        return getInt(APP_PREFERENCE, key, 1);
    }

    public static int getInt(String key, int defaultValue) {
        return getInt(APP_PREFERENCE, key, defaultValue);
    }

    public static int getInt(String preferenceName, String key, int defaultValue) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        return pref.getInt(key, defaultValue); // -1 means reloading previous
                                               // session
    }

    // ----------- setInt -----------
    public static void setInt(String key, int value) {
        setInt(APP_PREFERENCE, key, value);
    }

    public static void setInt(String preferenceName, String key, Integer value) {
        if(value == null) {
            BasePreferences.removePreferenceKey(preferenceName, key);
        } else {
            Editor editor = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS).edit();
            editor.putInt(key, value);
            editor.apply();
        }
    }

    public static void incrementInt(String key, int value) {
        incrementInt(APP_PREFERENCE, key, value);
    }

    public static void incrementInt(String preferenceName, String key, int value) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        Editor editor = pref.edit();
        editor.putInt(key, pref.getInt(key, 0) + value);
        editor.apply();
    }

    // ----------- getString -----------
    public static String getString(String key) {
        return getString(APP_PREFERENCE, key, "");
    }

    public static String getString(String key, String defaultValue) {
        return getString(APP_PREFERENCE, key, defaultValue);
    }

    public static String getString(String preferenceName, String key, String defaultValue) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        return pref.getString(key, defaultValue);
    }

    // ----------- setString -----------
    public static void setString(String key, String value) {
        setString(APP_PREFERENCE, key, value);
    }

    public static void setString(String preferenceName, String key, String value) {
        if(value == null) {
            BasePreferences.removePreferenceKey(preferenceName, key);
        } else {
            Editor editor = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS).edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public static void appendString(String key, String value) {
        setString(APP_PREFERENCE, key, value);
    }

    public static void appendString(String preferenceName, String key, String value) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        Editor editor = pref.edit();
        editor.putString(key, pref.getString(key, "") + value);
        editor.apply();
    }

    // ----------- getBoolean -----------
    public static boolean getBoolean(String key) {
        return getBoolean(APP_PREFERENCE, key, false);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return getBoolean(APP_PREFERENCE, key, defaultValue);
    }

    public static boolean getBoolean(String preferenceName, String key, boolean defaultValue) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        return pref.getBoolean(key, defaultValue); // -1 means reloading
                                                   // previous session
    }

    // ----------- setBoolean -----------
    public static void setBoolean(String key, boolean value) {
        setBoolean(APP_PREFERENCE, key, value);
    }

    public static void setBoolean(String preferenceName, String key, Boolean value) {
        if(value == null) {
            BasePreferences.removePreferenceKey(preferenceName, key);
        } else {
            Editor editor = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS).edit();
            editor.putBoolean(key, value);
            editor.apply();
        }
    }

    // ----------- setPreferenceKey -----------
    public static void removePreferenceKey(String key) {
        removePreferenceKey(APP_PREFERENCE, key);
    }

    public static void removePreferenceKey(String preferenceName, String key) {
        Editor editor = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS).edit();
        editor.remove(key);
        editor.apply();
    }

    public static void clearAppPreferences() {
        clearPreferences(BasePreferences.APP_PREFERENCE);
    }

    public static void clearPreferences(String preferenceName) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    public static boolean isContainsKey(String preferenceName, String key) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        return pref.contains(key);
    }
    public static boolean isContainsKey(String key) {
        return isContainsKey(APP_PREFERENCE, key);
    }

    // ----------- getLong -----------
    public static Long getLong(String key, Long defaultValue) {
        return getLong(APP_PREFERENCE, key, defaultValue);
    }

    public static Long getLong(String preferenceName, String key, Long defaultValue) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        if(defaultValue != null) {
            return pref.getLong(key, defaultValue);
        } else {
            Long result = pref.getLong(key, -1);
            return result == -1 ? defaultValue : result;
        }
    }

    // ----------- setLong -----------
    public static void setLong(String key, Long value) {
        setLong(APP_PREFERENCE, key, value);
    }

    public static void setLong(String preferenceName, String key, Long value) {
        if(value == null) {
            BasePreferences.removePreferenceKey(preferenceName, key);
        } else {
            Editor editor = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS).edit();
            editor.putLong(key, value);
            editor.apply();
        }
    }

    // ----------- getFloat -----------
    public static float getFloat(String key, float defaultValue) {
        return getFloat(APP_PREFERENCE, key, defaultValue);
    }

    public static float getFloat(String preferenceName, String key, float defaultValue) {
        SharedPreferences pref = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
        return pref.getFloat(key, defaultValue);
    }

    // ----------- setFloat -----------
    public static void setFloat(String key, float value) {
        setFloat(APP_PREFERENCE, key, value);
    }

    public static void setFloat(String preferenceName, String key, Float value) {
        if(value == null) {
            BasePreferences.removePreferenceKey(preferenceName, key);
        } else {
            Editor editor = BaseApplication.getInstance().getSharedPreferences(preferenceName, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS).edit();
            editor.putFloat(key, value);
            editor.apply();
        }
    }
}