package com.hackspace.alex.worklibrary.utils;

import android.support.v7.widget.GridLayoutManager;

import com.hackspace.alex.worklibrary.ui.helper.EndlessAdapter;

public final class EndlessListUtils {
    /**
     * Creates SpanSizeLookup for proper lookup of loading footer or error view.
     * @param adapter adapter need to know item type by position
     * @param spanCount spanCount need to set weight of error and loading footer item view
     * @return properly configured SpanSizeLookup
     */
    public static GridLayoutManager.SpanSizeLookup getProperSpanSizeLookup(
            EndlessAdapter adapter, int spanCount) {

        return new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int spanSize = 1; // by default
                switch (adapter.getItemViewType(position)) {
                    case EndlessAdapter.CONTENT_VIEW:
                        spanSize = 1;
                        break;

                    case EndlessAdapter.ERROR_FOOTER_VIEW:

                    case EndlessAdapter.LOADING_FOOTER_VIEW:

                        spanSize = spanCount;
                        break;

                    default:
                        // do nothing cause value by default set
                }
                return spanSize;
            }
        };
    }
}
