package com.hackspace.alex.worklibrary.utils;

import static com.hackspace.alex.worklibrary.utils.StringUtils.isEmpty;

import java.io.File;
import java.io.IOException;

public class LogX {
    public static boolean isDebug = true;
    private static int _10_MB = 10 * 1000 * 1000;
    public static final String DEFAULT_TAG = "crpm.txt";
    public static final String API_LOG = "api_log.txt";
    public static final String PROFILING = "profiling";
    public static final String DATABASE_LOG = "db_log.txt";
    public static final String EXCEPTION = "exception";
    public static final String WARNING = "warning";
    public static final String CONFIG = "config";

    public static final String LOG_FILE_PATH = "log.txt";
    public static final File LOG_FILE = FileManager.getAppSdFileByPath(LOG_FILE_PATH);

    public static void d(Object o) {
        d(DEFAULT_TAG, o);
    }

    public static void d(String tag, Object o) {
        d(tag, o, "");
    }

    private static String getClassNameAndLineNumber() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        // class that launches LogX.()
        int externalClassIndex = getExternalClassIndex(stackTrace);

        String fullClassName = stackTrace[externalClassIndex].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String fileName = stackTrace[externalClassIndex].getFileName();

        int lineNumber = stackTrace[externalClassIndex].getLineNumber();

        String methodName = Thread.currentThread().getStackTrace()[externalClassIndex].getMethodName();

        return new StringBuilder("    at ")
                                .append(className+"."+methodName)
                                .append("(")
                                .append(fileName)
                                .append(":")
                                .append(lineNumber)
                          .append(")")
                    .toString();
    }
    private static int getExternalClassIndex(StackTraceElement[] stackTrack) {
        String fullClassName = "";
        int index = 2;
        while(index < stackTrack.length) {
            fullClassName = stackTrack[index].getClassName();
            if(!fullClassName.equals(LogX.class.getName())) break;
            index++;
        }
        return index;
    }

    public static void d(String tag, Object o, String comment) {
        if (isDebug) {
            if(!isEmpty(comment)) comment = "[" + comment + "]";
            String txt = comment + (o != null ? o.toString() : "");
            writeLog(tag, "-d-" + DateHelper.getUtcTimeAsIsoString() + " : " + txt);
            android.util.Log.d(tag, txt+"\n"+getClassNameAndLineNumber());
        }
    }

    public static void w(String tag, Object o) {
        w(tag, o, "");
    }
    public static void w(Object o) {
        w(DEFAULT_TAG, o, "");
    }

    public static void w(String tag, Object o, String comment) {
        if (isDebug) {
            if(!isEmpty(comment)) comment = "[" + comment + "]";
            String txt = comment + (o != null ? o.toString() : "");
            writeLog(tag, "-w-" + DateHelper.getUtcTimeAsIsoString() + " : " + txt);
            android.util.Log.w(tag, txt+"\n"+getClassNameAndLineNumber());
        }
    }

    /*w - means warning. Means that data exist, but incorrect. Dannie est' no oni krivie. */
    public static void w(String tag, Exception e, boolean shouldNotifyDeveloper) {
        if (isDebug) {
            String comment = e.toString();
            String txt = "[" + comment + "]" + (e != null ? StringUtils.stack2string(e) : "");
            writeLog(tag, "-e-" + DateHelper.getUtcTimeAsIsoString() + " : " + txt);
            android.util.Log.w(tag, txt + "\n" + getClassNameAndLineNumber());
        }
    }

    public static void w(Exception e) {
        w(DEFAULT_TAG, e, false);
    }
    public static void w(Exception e, boolean shouldNotifyDeveloper) {
        w(DEFAULT_TAG, e, shouldNotifyDeveloper);
    }


    public static void d(File file, String comment) {
        if (isDebug && file != null) {
            if(!isEmpty(comment)) comment = "[" + comment + "]";
            String txt = comment +" file<" + file + "> " + (file.exists() ? "" : "doesn't") + " exist";
            writeLog(LOG_FILE, "-d-" + DateHelper.getUtcTimeAsIsoString() + " : " + txt);
            android.util.Log.d(file.getName(), txt + "\n"+getClassNameAndLineNumber());
        }
    }

    public static void e(Exception e) {
        e(DEFAULT_TAG, e);
    }

    public static void e(String tag, Exception e) {
        e(tag, e, e.toString());
    }

    public static void e(Exception e, String comment) {
        e(DEFAULT_TAG, e, comment);
    }

    public static void e(String tag, Exception e, String comment) {
        if (isDebug) {
            e.printStackTrace();
            if(!isEmpty(comment)) comment = "[" + comment + "]";
            String txt = comment + (e != null ? StringUtils.stack2string(e) : "");
            writeLog(tag, "-e-" + DateHelper.getUtcTimeAsIsoString() + " : " + txt);
            android.util.Log.e(tag, txt);
        }
    }

    public static void writeLog(String logFilePath, Object o) {
        writeLog(FileManager.getAppSdFileByPath(logFilePath), o);
    }

    public static void writeLog(File logFile, Object o) {
        if (isDebug) {
            try {
                if (logFile.exists() && logFile.length() > _10_MB) {
                    FileManager.moveFile(logFile, FileManager.getRenamedFileIsExists(logFile));
                }
                FileManager.createTextFile(logFile, "\n\n" + o, true);
            } catch (IOException e) {
                LogX.w(e, false);
            }
        }
    }

    public static String getStackTrace() {
        StringBuilder stackTraceStr = new StringBuilder();
        StackTraceElement stackElem;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for(int i = stackTrace.length-1; i > 2 ; i--) {
            stackElem = stackTrace[i];
            if(stackTraceStr.length() > 0) stackTraceStr.append("\n");
            stackTraceStr.append(String.format("   at %s(%s:%s)", stackElem.getMethodName(), stackElem.getFileName(),stackElem.getLineNumber()));
        }
        return stackTraceStr.toString();
    }
}
