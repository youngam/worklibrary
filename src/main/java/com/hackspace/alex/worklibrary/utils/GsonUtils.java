package com.hackspace.alex.worklibrary.utils;

import static java.lang.String.format;
import static java.util.Arrays.asList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import junit.framework.Assert;

import android.annotation.TargetApi;
import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.internal.bind.TypeAdapters;

public class GsonUtils {
    private static final Gson GSON = new Gson();

    public static Set<String> getJsonObjectFieldSet(JsonObject jsonObject) {
        Set<String> result = new LinkedHashSet<String>();
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
          result.add(entry.getKey());
        }
        return result;
    }

    public static void setJsonElementByPath(JsonObject parentObject, String newJsonFieldPath, JsonElement newElement) {
        int ind = newJsonFieldPath.indexOf(".");

        String fieldName = ind == -1 ? newJsonFieldPath : newJsonFieldPath.substring(0, ind);

        if(ind != -1) {
            String tailFieldName = newJsonFieldPath.substring(ind+1);
            if(parentObject.has(fieldName)) {
                JsonElement fieldValueElement = parentObject.get(fieldName);
                if(!fieldValueElement.isJsonObject()) Assert.fail(format("fieldName <%s> is already busy : %s", fieldName, fieldValueElement));
                setJsonElementByPath(fieldValueElement.getAsJsonObject(), tailFieldName, newElement);
            } else {
                JsonObject newJsonObject = new JsonObject();
                parentObject.add(fieldName, newJsonObject);
                setJsonElementByPath(newJsonObject, tailFieldName, newElement);
            }
        } else {
            if(parentObject.has(fieldName)) Assert.fail(format("fieldName <%s> is already busy : %s", fieldName, parentObject.get(fieldName)));
            parentObject.add(fieldName, newElement);
        }
    }


    public static JsonElement getJsonElementByPath(JsonObject jsonObject, Collection<String> nodes) throws IllegalArgumentException {
        return getJsonElementByPath(jsonObject, StringUtils.join(nodes, "."));
    }
    public static JsonElement getJsonElementByPath(JsonObject jsonObject, String jsonPath) throws IllegalArgumentException {
        int dotInd = jsonPath.indexOf(".");
        boolean hasDot = dotInd != -1;

        String headJsonName = hasDot ? jsonPath.substring(0, dotInd) : jsonPath;
        JsonElement headJsonElement = jsonObject.get(headJsonName);
//          assert fieldValueElement.isJsonObject() : format("fieldName <%s> is not object : %s", fieldName, fieldValueElement);
//          assert parentObject.has(fieldName) : format("fieldName <%s> is missing : %s", fieldName, parentObject);

        if(hasDot && headJsonElement != null) {
            if(!headJsonElement.isJsonObject()) throw new IllegalArgumentException(headJsonName + " is not an JsonObject");
            String tailPath = jsonPath.substring(dotInd+1);
            return getJsonElementByPath(headJsonElement.getAsJsonObject(), tailPath);

        } else return headJsonElement;
    }

    public static JsonArray createLongArray(long ... arr) {
        JsonArray array = new JsonArray();
        for(Number number : arr) {
            array.add(new JsonPrimitive(number));
        }
        return array;
    }

    public static JsonArray createLongArray(Collection<? extends Number> arr) {
        JsonArray array = new JsonArray();
        if(arr != null) {
            for(Number number : arr) {
                array.add(new JsonPrimitive(number));
            }
        }
        return array;
    }

    public static JsonArray createStringArray(Collection<String> arr) {
        JsonArray array = new JsonArray();
        if(arr != null) {
            for(String string : arr) {
                array.add(new JsonPrimitive(string));
            }
        }
        return array;
    }


    public static JsonObject newJsonObject(String property, JsonElement jsonElement)        {return addJson(new JsonObject(), property, jsonElement);}
    public static JsonObject newJsonObject(List<String> path, JsonElement jsonElement)      {return addJson(new JsonObject(), path, jsonElement);}

    public static JsonObject addJson(JsonObject jsonObject, String property, JsonElement jsonElement) {
        jsonObject.add(property, jsonElement);
        return jsonObject;
    }
    public static JsonObject addJson(JsonObject rootJson, List<String> path, JsonElement jsonElement) {
        JsonObject tmpJson;
        JsonObject iterJson = rootJson;
        for(int i = 0; i < path.size() ; ++i) {
            String property = path.get(i);
            if(i == path.size() - 1) iterJson.add(property, jsonElement);
            else if(iterJson.has(property)) iterJson = iterJson.get(property).getAsJsonObject();
            else {
                iterJson.add(property, tmpJson = new JsonObject());
                iterJson = tmpJson;
            }
        }
        return rootJson;
    }

    public static JsonObject newJsonObject(String property, Number value)                       {return addJson(new JsonObject(), asList(property), value);}
    public static JsonObject newJsonObject(List<String> path, Number value)                     {return addJson(new JsonObject(), path, value);}
    public static JsonObject addJson(JsonObject rootJson, String property, Number value)        {return addJson(rootJson, asList(property), value);}
    public static JsonObject addJson(JsonObject rootJson, List<String> path, Number value)      {return addJson(rootJson, path, new JsonPrimitive(value));}

    public static JsonObject newJsonObject(String property, String value)                       {return addJson(new JsonObject(), asList(property), value);}
    public static JsonObject newJsonObject(List<String> path, String value)                     {return addJson(new JsonObject(), path, value);}
    public static JsonObject addJson(JsonObject rootJson, String property, String value)        {return addJson(rootJson, asList(property), value);}
    public static JsonObject addJson(JsonObject rootJson, List<String> path, String value)      {return addJson(rootJson, path, new JsonPrimitive(value));}

    public static JsonObject newJsonObject(String property, boolean value)                      {return addJson(new JsonObject(), asList(property), value);}
    public static JsonObject newJsonObject(List<String> path, boolean value)                    {return addJson(new JsonObject(), path, value);}
    public static JsonObject addJson(JsonObject rootJson, String property, boolean value)       {return addJson(rootJson, asList(property), value);}
    public static JsonObject addJson(JsonObject rootJson, List<String> path, boolean value)     {return addJson(rootJson, path, new JsonPrimitive(value));}

    public static JsonObject newJsonObject(List<String> path, Collection<Long> ids)                {return addJson(new JsonObject(), path, ids);}
    public static JsonObject newJsonObject(String path, Collection<Long> ids)                {return addJson(new JsonObject(), asList(path), ids);}
    public static JsonObject addJson(JsonObject rootJson, List<String> path, Collection<Long> ids) {return addJson(rootJson, path, createLongArray(ids));}


    public static ArrayList<Long> getLongSet(JsonArray idsArray) {
        ArrayList<Long> result = new ArrayList<Long>();

        for (int i = 0; i < idsArray.size(); ++i) {
            JsonElement idElem = idsArray.get(i);
            if(idElem == null || !idElem.isJsonPrimitive()) throw new IllegalArgumentException("Array of [id1, id2,...] is expected but found : " + idsArray);

            result.add(idElem.getAsLong());
        }
        return result;
    }

    public static void joinAndReplace(JsonObject json, JsonObject json2) {
        for(Entry<String, JsonElement> jsonEntry :json2.entrySet()) {
            String sourceFieldName = jsonEntry.getKey();
            JsonElement sourceFieldJson = jsonEntry.getValue();


            if(!json.has(sourceFieldName)) {
                json.add(sourceFieldName, sourceFieldJson);
            } else {
                JsonElement distJson = json.get(sourceFieldName);
                if(distJson.equals(sourceFieldJson)) continue;

                JsonType distType = getType(distJson);
                if(distType != getType(sourceFieldJson)) throw new IllegalArgumentException();
                if(distType == JsonType.ARRAY) {
                    addAllToArray(distJson.getAsJsonArray(), sourceFieldJson.getAsJsonArray());

                } else if(distType == JsonType.OBJECT) {
                    joinAndReplace(distJson.getAsJsonObject(), sourceFieldJson.getAsJsonObject());

                } else {
                    json.add(sourceFieldName, sourceFieldJson); // old value will be replaced;
                }
            }
        }
    }

    private static void addAllToArray(JsonArray array1, JsonArray array2) {
        for(JsonElement elem :array2) array1.add(elem);
    }

    private static enum JsonType {
        NULL, STRING, OBJECT, ARRAY,
        NUMBER, BOOLEAN
    };

    private static JsonType getType(JsonElement elem) {
        if(elem == null) return null;
        JsonType type = null;

        if(elem.isJsonArray()) type = JsonType.ARRAY;
        else if(elem.isJsonNull()) type = JsonType.NULL;
        else if(elem.isJsonObject()) type = JsonType.OBJECT;
        else if(elem.isJsonPrimitive()) {
            JsonPrimitive primitive = (JsonPrimitive) elem;

            if(primitive.isBoolean()) type = JsonType.BOOLEAN;
            else if(primitive.isNumber()) type = JsonType.NUMBER;
            else if(primitive.isString()) type = JsonType.STRING;
            else throw new ClassCastException();
        } else throw new IllegalArgumentException();

        return type;
    }

    /**
     * Parse {@link File} and return a {@link JsonObject}.
     * @param fileWithJson The {@link File} what contain a json
     * @return return a {@link JsonObject}
     * @throws IOException
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static JsonObject getFileAsJson(File fileWithJson) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (FileInputStream fin = new FileInputStream(fileWithJson)){
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return (JsonObject) TypeAdapters.JSON_ELEMENT.fromJson(sb.toString());
    }

    public static Gson getGson() {
        return GSON;
    }
}
