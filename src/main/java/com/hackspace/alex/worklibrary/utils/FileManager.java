package com.hackspace.alex.worklibrary.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;

import com.hackspace.alex.worklibrary.BaseApplication;

public class FileManager {
    public static final String CRPM_FOLDER = "crpm/";
    public static final File APP_PATH;
    public static final File INTERNAL_DEVICE_APP_PATH;
    protected static String APP_DIR;

    protected static final File SD_ROOT = Environment.getExternalStorageDirectory();
    protected static final String SD_PATH = SD_ROOT.getAbsolutePath() + "/";
    private static final Context APP_CONTEXT = BaseApplication.getInstance();

    static {
        INTERNAL_DEVICE_APP_PATH = APP_CONTEXT.getFilesDir().getParentFile();

        APP_PATH = getSdFileByPath(CRPM_FOLDER);
        if (!APP_PATH.exists()) {
            APP_PATH.mkdir();
        }
        APP_DIR = APP_PATH.getAbsolutePath() + "/";

        // VOICE_MESSAGES_PATH = SD_APP_PATH + VOICE_MESSAGES_DIR_NAME;
    }

    public static boolean moveFile(File from, File to) throws IOException {
        if (from.equals(to)) return true;
        IOException exception = null;
        try {
            copyFile(from, to);
        } catch (IOException e) {
            e.printStackTrace();
            exception = e;
        }
        if (exception == null) {
            boolean deleteStatus = from.delete();
            LogX.d("moveFile(" + from + "," + to + "). Delete: " + from + " : " + deleteStatus);
            return deleteStatus;
        } else {
            throw exception;
        }

    }

    public static boolean copyFile(File from, File to) throws IOException {
        if (from.equals(to)) return true;
        FileInputStream in = new FileInputStream(from.getAbsoluteFile());
        FileOutputStream out = new FileOutputStream(to.getAbsolutePath());
        try {
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
        return true;
    }

    public static File[] getDirFiles(String folderPath, final String extension) {
        return getDirFiles(new File(folderPath), extension);
    }

    public static File[] getDirFiles(File folder, final String extension) {
        if (!folder.isDirectory()) {
            return new File[]{};
        } else {
            return folder.listFiles(new FileFilter() {
                public boolean accept(File file) {
                    return file.getName().endsWith(extension);
                }
            });
        }
    }

    /**
     * Retrieve or creates <b>path</b> inside of <b>dir</b> folder
     *
     * @param dir
     * @param path
     * @return
     */
    private static File getChildrenFolder(File dir, String path) {
        List<String> dirs = new ArrayList<String>(Arrays.<String>asList(path.split("/")));
        for (int i = 0; i < dirs.size(); ++i) {
            dir = new File(dir, dirs.get(i)); // Getting a file within the dir.
            if (!dir.exists()) {
                dir.mkdir();
            }
        }
        return dir;
    }

    public static boolean deleteDir(File dir) {
        return deleteDir(dir, true);
    }

    public static boolean deleteDir(File dir, boolean deleteSubFolders) {
        boolean deleteStatus = dir.exists();
        if (dir.exists()) {
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    File childFile = new File(dir, children[i]);

                    if (childFile.isFile() || (childFile.isDirectory() && deleteSubFolders)) {
                        boolean isDirDeleted = deleteDir(childFile, deleteSubFolders);
                        if (!isDirDeleted) {
                            LogX.d("deleteDir(" + dir + ") can't be executed ");
                            deleteStatus = false;

                            break;
                        }
                    } else {
                        deleteStatus = false;
                    }
                }
            }
            if (deleteStatus) {
                deleteStatus = dir.delete();
                LogX.d("deleteDir(" + dir + ") was " + deleteStatus);
            }
        } else {
            LogX.d("deleteDir(" + dir + ") file doesn't exist ");
        }

        return deleteStatus;
    }

    /**
     * Local Application Path were made by creating of folder inside of
     * application context.
     *
     * @param path Local Application Path to file: "dir1/dir2/file.txt"
     * @return
     */
    public static File getAppFileByPath(String path) {
        if (path.startsWith(APP_DIR)) {
            path = path.substring(APP_DIR.length());
        }

        if (path.indexOf("/") == -1) { // the file name only (no directories)
            return new File(APP_CONTEXT.getFilesDir(), path);
        } else {
            String fileName = path.substring(path.lastIndexOf("/") + 1);
            path = path.substring(0, path.lastIndexOf("/"));
            File fileDir = getChildrenFolder(APP_PATH, path);
            return new File(fileDir, fileName);
        }
    }

    /**
     * Local Application Path were made by creating of folder inside of
     * application context.
     *
     * @param path Local Application Path to file: "dir1/dir2/file.txt"
     * @return
     */
    public static File getSdFileByPath(String path) {
        if (path.indexOf("/") == -1) {
            return new File(SD_ROOT, path);
        } else {
            String fileName = path.substring(path.lastIndexOf("/") + 1);
            path = path.substring(0, path.lastIndexOf("/"));
            File fileDir = getChildrenFolder(SD_ROOT, path);
            return new File(fileDir, fileName);
        }
    }

    /**
     * Local Application Path were made by creating of folder inside of
     * application context.
     *
     * @param path Local Application Path to file: "dir1/dir2/file.txt"
     * @return
     */
    public static File getAppSdFileByPath(String path) {
        return getSdFileByPath(CRPM_FOLDER + path);
    }

    /**
     * Retrieves or creates FileOutputStream of given path
     *
     * @param path Example: "dir1/dir2/dir3/file.xml"
     * @return fos : "file.xml"
     * @throws FileNotFoundException
     */
    public static FileOutputStream openAppFileOutput(String path) throws FileNotFoundException {
        File destinationFile = getAppFileByPath(path);
        return new FileOutputStream(destinationFile);
    }

    public static FileInputStream openAppFileInput(String path) throws FileNotFoundException {
        File destinationFile = getAppFileByPath(path);
        return new FileInputStream(destinationFile);
    }

    public static File createTextAppFile(String path, String text) throws IOException {
        return createTextFile(getAppFileByPath(path), text, false);
    }

    public static File createTextAppFile(String path, String text, boolean append) throws IOException {
        return createTextFile(getAppFileByPath(path), text, append);
    }

    public static File createTestTextSdFile(String path, boolean append) throws IOException {
        return createTextFile(getSdFileByPath(path), "hello", append);
    }

    public static File createTextSdFile(String path, String txt) throws IOException {
        return createTextSdFile(path, txt, false);
    }

    public static File createTextSdFile(String path, String txt, boolean append) throws IOException {
        return createTextFile(getSdFileByPath(path), txt, append);
    }

    public static File createTextFile(File file, String text, boolean append) throws IOException {
        FileOutputStream out = new FileOutputStream(file, append);
        OutputStreamWriter writer = new OutputStreamWriter(out);
        writer.write(text);
        writer.close();
        return file;
    }

    public static String getFileExtention(File file) {
        return getFileExtention(file.toString());
    }

    public static File getChangedExtensionFile(File file, String newExtention) {
        String newFilePath = getChangedExtentionFilePath(file.getAbsolutePath(), newExtention);
        File newFile = getFileByPath(newFilePath);
        return newFile;
    }

    private static String getChangedExtentionFilePath(String filePath, String newExtention) {
        String newExtFilePath;

        String ext = getFileExtention(filePath);

        if (!StringUtils.isEmpty(ext)) {
            newExtFilePath = StringUtils.replaceLast(filePath, ext, newExtention);
        } else {
            newExtFilePath = filePath + (filePath.endsWith(".") ? "" : ".") + newExtention;
        }

        return newExtFilePath;
    }

    public static String getFileExtention(String path) {
        int dotIndex = path.lastIndexOf(".");
        int slashIndex = path.lastIndexOf("/");
        return (dotIndex != -1 && dotIndex > slashIndex) ? path.substring(dotIndex + 1) : "";
    }

    /**
     * If file "hello.jpg" exists, this function returns "hello-1.jpg" or
     * "hello-2.jpg" and so on...
     *
     * @param file
     * @return
     */
    public static File getRenamedFileIsExists(File file) {
        if (!file.exists()) {
            return file;
        }
        File newFile = new File(file.toString());
        int i = 1;
        String filePath = file.toString();
        int filePathLastIndex = filePath.lastIndexOf(".");
        String ext = filePathLastIndex == -1 ? "" : filePath.substring(filePathLastIndex);
        String name = filePathLastIndex == -1 ? filePath.substring(0, filePath.length()) : filePath.substring(0, filePathLastIndex);
        while (newFile.exists()) {
            newFile = new File(name + "-" + i++ + ext);
        }
        return newFile;
    }

    private static byte[] createChecksum(String filename) throws IOException {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = null;
        try {
            complete = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            // impossible. MD5 Must be
            LogX.e(e);
        }
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    public static String getMD5Checksum(File file) throws IOException {
        return getMD5Checksum(file.toString());
    }

    // see this How-to for a faster way to convert
    // a byte array to a HEX string
    public static String getMD5Checksum(String filePath) throws IOException {
        byte[] b = createChecksum(filePath);
        String result = "";

        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static File createFile(InputStream is, String path) {
        File f = null;
        try {
            f = new File(path);
            FileOutputStream out = new FileOutputStream(f);
            byte buf[] = new byte[1024];
            int len;
            while ((len = is.read(buf)) > 0)
                out.write(buf, 0, len);
            out.close();
            is.close();
        } catch (IOException e) {
            LogX.w(e);
        }
        return f;
    }

    public static byte[] getFileBytes(String filePath) throws IOException {
        return getFileBytes(new File(filePath));
    }

    public static byte[] getFileBytes(File file) throws IOException {
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(file);
            byte fileContent[] = new byte[(int) file.length()];
            fin.read(fileContent);
            return fileContent;
        } finally {
            if (fin != null) fin.close();
        }
    }

    public static String getFileNameOnly(File file) {
        if (file == null) {
            return "";
        } else {
            String fileName = file.getName();
            int dotIndex = fileName.toString().lastIndexOf(".");
            if (dotIndex == -1) {
                return fileName;
            } else {
                return fileName.substring(0, dotIndex);
            }
        }
    }

    public static boolean isEmptyFile(File file) {
        return file == null || !file.exists() || file.length() == 0;
    }

    public static boolean isEmptyFile(String filePath) {
        return StringUtils.isEmpty(filePath) || isEmptyFile(new File(filePath));
    }

    public static File getFileByPath(File file) {
        File securedFile;
        String path = file.getAbsolutePath();
        if (path.startsWith(APP_DIR)) {
            path = path.substring(APP_DIR.length());
            securedFile = getAppFileByPath(path);
        } else {
            securedFile = getSdFileByPath(path);
        }
        return securedFile;
    }

    public static File getFileByPath(String path) {
        File file;
        if (path.startsWith(APP_DIR)) {
            path = path.substring(APP_DIR.length());
            file = getAppFileByPath(path);
        } else {
            file = getSdFileByPath(path);
        }
        return file;
    }

    public static String readFileToString(File file) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            String result = StringUtils.inputStreamToString(fis);
            return result;
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }

    public static String readFileToString64(String filePath) throws IOException {
        return readFileToString64(new File(filePath));
    }

    public static String readFileToString64(File file) throws IOException {
        byte[] fileBinary = getFileBytes(file);
        return Base64.encodeToString(fileBinary, Base64.NO_WRAP);
    }

    public static File writeStringToFile(String text, File file) throws IOException {
        FileOutputStream fos = null;
        OutputStreamWriter writer = null;
        try {
            fos = new FileOutputStream(file);
            writer = new OutputStreamWriter(fos);
            writer.write(text);

            return file;
        } finally {
            writer.close();
            fos.close();
        }
    }

    public static File convertStringToFile(String filePath, String data) throws IOException {
        FileOutputStream fos = null;
        try {
            File file = FileManager.getAppSdFileByPath(filePath);
            fos = new FileOutputStream(file);
            fos.write(data.getBytes("UTF-8"));
            if (fos != null) fos.close();

            file = FileManager.getAppSdFileByPath(filePath + "_");
            fos = new FileOutputStream(file);
            fos.write(data.getBytes());

            return file;
        } finally {
            if (fos != null) fos.close();
        }
    }

    public static File writeBytesToFile(byte[] bytes, File file) throws IOException {
        OutputStream output = null;
        try {
            output = new BufferedOutputStream(new FileOutputStream(file));
            output.write(bytes);

            return file;
        } finally {
            output.close();
        }
    }

    public static void deleteVoiceFile(String path) {
        File outputFile = new File(path);
        if (outputFile.exists()) {
            outputFile.delete();
        }
    }

    public static boolean isExistVoiceFile(String path) {
        File outputFile = new File(path);
        return outputFile.exists();
    }

    public static String generateFileName() {
        return new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.ENGLISH).format(new Date());
    }

    public static String getVideoPathFromURI(Uri contentUri) {
        Context context = APP_CONTEXT;
        Cursor cursor = null;

        try {

            String[] proj = {MediaStore.Video.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);

        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean isAssertExist(String localPath) {
        if (localPath == null) throw new IllegalArgumentException("localPath is null");
        String fileNameAndExt = fetchFileNameAndExt(localPath);
        if (fileNameAndExt.length() == 0) return false;

        String localFolder = fetchFolderPath(localPath);
        String[] folderFiles;
        try {
            folderFiles = APP_CONTEXT.getAssets().list(localFolder);
        } catch (IOException e) {
            return false;
        }
        for (String filePath : folderFiles) {
            if (fileNameAndExt.equalsIgnoreCase(filePath)) return true;
        }
        return false;
    }

    private static String fetchFolderPath(String filePath) {
        int index = filePath.lastIndexOf("/");
        if (index == -1) index = filePath.lastIndexOf("\\");

        if (index == -1) return "";
        else return filePath.substring(0, index);
    }

    private static String fetchFileNameAndExt(String filePath) {
        int index = filePath.lastIndexOf("/");
        if (index == -1) index = filePath.lastIndexOf("\\");

        if (index == -1 && filePath.length() >= 0) return filePath;
        else return filePath.substring(index + 1);
    }
}
