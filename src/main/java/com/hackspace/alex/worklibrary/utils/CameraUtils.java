package com.hackspace.alex.worklibrary.utils;

import android.app.Activity;
import android.content.Intent;

public final class CameraUtils {
    private CameraUtils() {

    }

    public static void pickImage(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(intent, requestCode);
    }
}
