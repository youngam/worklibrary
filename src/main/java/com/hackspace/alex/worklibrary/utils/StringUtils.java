package com.hackspace.alex.worklibrary.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.widget.EditText;
import android.widget.TextView;

import com.hackspace.alex.worklibrary.BaseApplication;


public class StringUtils {
    public static final String EMPTY_STRING = "";
    private static Context sContext = BaseApplication.getInstance();

    /** Removes Multiline comments*/
    public static String removeComments(String text) {
        text = text.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)", "");
        return text;
    }

    /** Read content of file located by <u>path</u> to String text*/
    public static String readAssetFile(String path) throws IOException {
        InputStream is = sContext.getAssets().open(path);
        return inputStreamToString(is);
    }

    public static String inputStreamToString(InputStream is) throws IOException {
        /*
         * To convert the InputStream to String we use the
         * BufferedReader.readLine() method. We iterate until the BufferedReader
         * return null which means there's no more data to read. Each line will
         * appended to a StringBuilder and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } finally {
            if(is != null) is.close();
        }
        return sb.toString();
    }

    /** @return printed formatted StackTrace (with line breaks)*/
    public static String stack2string(Exception e) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return "------\r\n" + sw.toString() + "------\r\n";
        } catch (Exception e2) {
            return "bad stack2string";
        }
    }
    /** @return printed StackTrace (single line)*/
    public static String stackExceptionToSingleString(Exception e) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString().replaceAll("\n|\r", "");
        } catch (Exception e2) {
            return "bad stack2string";
        }
    }

    public static boolean isEmpty(CharSequence s) {
        return s == null || s.length() == 0;
    }

    /** Checks is TextView.getText().isEmpty()*/
    public static boolean isEmpty(TextView tv) {
        return isEmpty(tv.getText().toString());
    }

    /** converts ["text1", "text2",...] & delimiter ->  "text1{delimiter}text2{delimiter}..."*/
    public static String join(String delimiter, String... arr) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; ++i) {
            builder.append(arr[i]);
            if (i+1 == arr.length) break;
            else builder.append(delimiter);
        }
        return builder.toString();
    }

    /** converts ["text1", "text2",...] & delimiter ->  "text1{delimiter}text2{delimiter}..."*/
    public static String join(Collection<?> s, String delimiter) {
        if(s.isEmpty()) return "";

        StringBuilder builder = new StringBuilder();
        Iterator<?> iter = s.iterator();
        while (iter.hasNext()) {
            builder.append(iter.next());
            if (!iter.hasNext()) {
                break;
            }
            builder.append(delimiter);
        }
        return builder.toString();
    }

    public static int fetchEditTextInteger(EditText editText) {
        return fetchInteger(editText.getText().toString());
    }

    public static int fetchInteger(CharSequence text) {
        int result = 0;
        try {
            result = Integer.parseInt(text.toString().replaceAll("[^\\d]", ""));
        } catch (Exception e) {
        }
        return result;
    }

    /** Generate UUID.randomUUID and removes "-"
     * @see java.util.UUID#randomUUID()*/
    public static String getRandomString() {
        String str = java.util.UUID.randomUUID().toString();
        str = str.replaceAll("[\\d-]", "");
        return str;
    }

    public static int getRandomInteger(int fromRange, int toRange) {
        if (fromRange > toRange) {
            int temp = fromRange;
            fromRange = toRange;
            toRange = temp;
        }
        // get the range, casting to long to avoid overflow problems
        long range = (long) toRange - (long) fromRange + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * new Random().nextDouble());
        return (int) (fraction + fromRange);
    }

    public static String upperFirstLetter(String input) {
        if (!isEmpty(input)) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        } else {
            return input;
        }
    }

    /** Searches in <u>text</u> the last substring of <u>from</u> and replaces it by <u>to</u>*/
    public static String replaceLast(String text, String from, String to) {
        int lastIndex = text.lastIndexOf(from);
        if (lastIndex < 0) return text;
        String tail = text.substring(lastIndex).replaceFirst(from, to);
        return text.substring(0, lastIndex) + tail;
    }

    public static String escapeQuotes(String query) {
        StringBuilder escaper = new StringBuilder();
        if (query.indexOf('\'') != -1) {
            int length = query.length();
            for (int i = 0; i < length; i++) {
                char c = query.charAt(i);
                if (c == '\'') {
                    escaper.append('\'');
                }
                escaper.append(c);
            }
            query = escaper.toString();
        }
        return query;
    }

    /**
     * @param text
     * @param replacement
     * @return <ul>
     * <li>null - if text is null </li>
     * <li><b>replacement</b>, else returns itself. </li>
     * </ul>
     */
    public static String safeString(Object text, String replacement) {
        return text != null ? String.valueOf(text) : replacement;
    }

    public static String formatReplaceNullAsEmptyStr(String text, Object... replacements) {
        for(int i = 0; i < replacements.length; ++i) {
            if(replacements[i] == null) replacements[i] = "";
        }
        return String.format(text, replacements);
    }

    /** ignoreCase is set FALSE by default*/
    public static CharSequence highlightStringMatch(String targetString, String toMatch, int highlightColor) {
        return highlightStringMatch(targetString, toMatch, highlightColor, false);
    }
    public static CharSequence highlightStringMatch(String targetString, String toMatch, int highlightColor, boolean ignoreCase) {
        if (toMatch == null || toMatch == EMPTY_STRING) {
            return targetString;
        }

        int startIndex = ignoreCase ? targetString.toLowerCase().indexOf(toMatch.toLowerCase()) : targetString.indexOf(toMatch);
        //if start index -1 there is no match.
        if(startIndex != -1) {
            int lastIndex = startIndex+toMatch.length();

            Spannable spannableHighlight = new SpannableString(targetString);
            spannableHighlight.setSpan(new ForegroundColorSpan(highlightColor), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableHighlight.setSpan(new UnderlineSpan(), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableHighlight.setSpan(new StyleSpan(Typeface.BOLD), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return spannableHighlight;

        } else return targetString;
    }

    public static String capsFirstWordCharacter(String string) {
        return (!isEmpty(string)) ? string.substring(0, 1).toUpperCase() + string.substring(1) : "";
    }

    public static String getCapitalizedCharacters(String text, int startIndex, int endIndex) {
        return (!isEmpty(text)) ? text.substring(startIndex, endIndex).toUpperCase() + text.substring(1) : text;
    }

    public static boolean equals(String string1, String string2) {
        if(string1 == null && string2 == null) return true;
        else if(string1 == null || string2 == null) return false;
        else return string1.equals(string2);
    }

    public static boolean equalsIgnoreCase(String string1, String string2) {
        if(string1 == null && string2 == null) return true;
        else if(string1 == null || string2 == null) return false;
        else return string1.equalsIgnoreCase(string2);
    }

    public static String repeat(String s, int n) {
        if(s == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++) {
            sb.append(s);
        }
        return sb.toString();
    }

    public static int compareTo(String str1, String str2, boolean ascendingOrder) {
        return compareTo(str1, str2, ascendingOrder, false);
    }

    public static int compareTo(String str1, String str2, boolean ascendingOrder, boolean ignoreCase) {
        int result;
        if (str1 != null && str2 != null) {
            if (ignoreCase) result = str1.compareToIgnoreCase(str2) * (ascendingOrder? 1 : -1);
            else result = str1.compareTo(str2) * (ascendingOrder? 1 : -1);
        } else if (str1 != null) {
            result = ascendingOrder ? 1:-1;
        } else if (str2 != null) {
            result = ascendingOrder ? -1:1;
        } else { // both str1 == null && str2 == null
            result = 0; //
        }

        return result;
    }
}