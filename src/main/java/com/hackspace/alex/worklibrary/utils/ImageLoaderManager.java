package com.hackspace.alex.worklibrary.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.hackspace.alex.worklibrary.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

public class ImageLoaderManager {
    private static ImageLoaderManager IMAGE_LOADER = new ImageLoaderManager();
    private ImageLoader mImageLoader = ImageLoader.getInstance();
    private DisplayImageOptions mImageLoaderOptions;

    private ImageLoaderManager() {}

    public static ImageLoaderManager getInstance() {
        return IMAGE_LOADER;
    }

    public void init(Context context, int emptyDrawable, int errorDrawable) {

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();

        mImageLoader.init(config);

        mImageLoaderOptions = getDisplayOptions(emptyDrawable, errorDrawable)
                .build();
    }

    private DisplayImageOptions.Builder getDisplayOptions(int emptyDrawable, int errorDrawable) {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.transparent_logo)
                .showImageForEmptyUri(emptyDrawable)
                .showImageOnFail(errorDrawable)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())//magic don't touch it (This needed to scale ImageView properly)!!!
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565);
    }

    public void displayImage(String fileUrl, ImageView view) {
        mImageLoader.displayImage(fileUrl, view, mImageLoaderOptions);
    }
}
